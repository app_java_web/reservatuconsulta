package com.app_java_we.myapplication.repositories;

import android.content.Context;

import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.models.Turn;

import java.util.ArrayList;

public class TurnRepository {

    private Context mContext;
    private static TurnRepository  mTurnRepository = null;
    private ArrayList<Turn> turns;
    private HospitalAPI hospitalAPI;

    protected TurnRepository(Context mContext) {
        this.mContext = mContext;
        this.turns = new ArrayList<Turn>();
    }

    public static synchronized TurnRepository getInstance(Context mContext){
        if (mTurnRepository == null){
            mTurnRepository = new TurnRepository(mContext);
        }
        return mTurnRepository;
    }

    public ArrayList<Turn> getTurns() {
        return this.turns;
    }

    public void setTurns(ArrayList<Turn> turns) {
        this.turns = turns;
    }
}
