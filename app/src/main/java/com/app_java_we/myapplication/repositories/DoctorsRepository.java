package com.app_java_we.myapplication.repositories;

import android.content.Context;

import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.models.Doctor;

import java.util.ArrayList;

public class DoctorsRepository {

    private Context mContext;
    private static DoctorsRepository mDoctorsRepository = null;
    private ArrayList<Doctor> doctors;
    private ArrayList<Doctor> doctorsByID;
    private HospitalAPI hospitalAPI;

    protected DoctorsRepository(Context mContext) {
        this.mContext = mContext;
        this.doctors = new ArrayList<Doctor>();
    }

    public static synchronized DoctorsRepository getInstance(Context mContext){
        if (mDoctorsRepository == null){
            mDoctorsRepository = new DoctorsRepository(mContext);
        }
        return mDoctorsRepository;
    }

    public ArrayList<Doctor> getDoctors() {
        return this.doctors;
    }

    public void setDoctors(ArrayList<Doctor> doctors) {
        this.doctors = doctors;
    }

    public ArrayList<Doctor> getDoctorsByID() {
        return doctorsByID;
    }

    public void setDoctorsByID(ArrayList<Doctor> doctorsByID) {
        this.doctorsByID = doctorsByID;
    }

    public ArrayList<Doctor> getDoctorsBySpeciality(Long id){
        hospitalAPI = new HospitalAPI(mContext);
        hospitalAPI.getDoctorsBySpeciality(id);
        return this.doctors;
    }

    public ArrayList<Doctor> getDoctorBySpeciality2(Long id){
        doctorsByID = new ArrayList<>();
        for (int x = 0; x < doctors.size();x++){
            if (doctors.get(x).getSpeciality().getId().equals(id)){
                doctorsByID.add(doctors.get(x));
            }
        }
        return doctorsByID;
    }

}
