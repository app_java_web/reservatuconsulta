package com.app_java_we.myapplication.repositories;

import android.content.Context;

import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.models.Doctor;
import com.app_java_we.myapplication.models.Speciality;

import java.util.ArrayList;

public class SpecialtyRepository {

    private Context mContext;
    private static SpecialtyRepository  mSpecialtyRepository = null;
    private ArrayList<Speciality> specialities;
    private HospitalAPI hospitalAPI;

    protected SpecialtyRepository(Context mContext) {
        this.mContext = mContext;
        this.specialities = new ArrayList<Speciality>();
    }

    public static synchronized SpecialtyRepository getInstance(Context mContext){
        if (mSpecialtyRepository == null){
            mSpecialtyRepository = new SpecialtyRepository(mContext);
        }
        return mSpecialtyRepository;
    }

    public ArrayList<Speciality> getSpecialties() {
        return this.specialities;
    }

    public void setSpecialities(ArrayList<Speciality> specialities) {
        this.specialities = specialities;
    }

}
