package com.app_java_we.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.models.Speciality;

import java.util.List;

public class SpecialtyAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private int layout;
    private List<Speciality> specialityList;

    public SpecialtyAdapter(Context context, int layout, List<Speciality> specialityList) {
        this.context = context;
        this.layout = layout;
        this.specialityList = specialityList;
    }

    @Override
    public int getCount() {return this.specialityList.size();}

    @Override
    public Object getItem(int position) {return this.specialityList.get(position);}

    @Override
    public long getItemId(int ID) {return ID;}

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;

        if (convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(R.layout.list_item_specialty,null);

            holder =  new ViewHolder();

            holder.textView_specialty_title = (TextView) convertView.findViewById(R.id.textView_specialty_title);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Context context = viewGroup.getContext();
        View view = convertView;

        Speciality currentSpeciality = specialityList.get(position);
        holder.textView_specialty_title.setText(currentSpeciality.getName());

        return view;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    static class ViewHolder{
        private TextView textView_specialty_title;
    }
}
