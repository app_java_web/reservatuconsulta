package com.app_java_we.myapplication.api.callbacks;

import android.support.annotation.NonNull;

import com.app_java_we.myapplication.models.User;

public interface LoginUserCallback {

    void onSuccess(@NonNull User user);

    void onError(@NonNull Throwable throwable);
}
