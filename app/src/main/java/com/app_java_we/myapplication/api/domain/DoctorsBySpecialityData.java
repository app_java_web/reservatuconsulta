package com.app_java_we.myapplication.api.domain;

import com.app_java_we.myapplication.models.Doctor;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DoctorsBySpecialityData {

    @SerializedName("doctorList")
    private ArrayList<Doctor> doctors;

    public ArrayList<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(ArrayList<Doctor> doctors) {
        this.doctors = doctors;
    }
}
