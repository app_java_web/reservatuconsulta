package com.app_java_we.myapplication.api.callbacks;

import android.support.annotation.NonNull;

import com.app_java_we.myapplication.models.Turn;

import java.util.ArrayList;
import java.util.List;

public interface TurnsAvailableCallback {

    void onSuccess(@NonNull ArrayList<Turn> turn);

    void onError(@NonNull Throwable throwable);

}
