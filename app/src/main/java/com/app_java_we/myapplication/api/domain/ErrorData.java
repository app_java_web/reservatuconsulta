package com.app_java_we.myapplication.api.domain;

import java.util.Date;

public class ErrorData {
    private Date timestamp;
    private int status;
    private String error;
    private String message;
    private String trace;

    public ErrorData(Date timestamp, int status, String error, String message, String trace) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.trace = trace;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }
}
