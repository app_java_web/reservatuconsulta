package com.app_java_we.myapplication.api.domain;

import com.app_java_we.myapplication.models.Speciality;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SpecialitiesData {

    @SerializedName("specialtyList")
    private ArrayList<Speciality> specialities;

    public ArrayList<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<Speciality> specialities) {
        this.specialities = specialities;
    }
}
