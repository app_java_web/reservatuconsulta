package com.app_java_we.myapplication.api.callbacks;

import android.support.annotation.NonNull;

import com.app_java_we.myapplication.models.User;

public interface GetUserCallback {

    void onSuccess(@NonNull User user);

    void onError(@NonNull Throwable throwable);
}
