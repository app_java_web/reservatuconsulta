package com.app_java_we.myapplication.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.api.callbacks.GetUserCallback;
import com.app_java_we.myapplication.api.callbacks.LoginUserCallback;
import com.app_java_we.myapplication.api.callbacks.PostTurnCallback;
import com.app_java_we.myapplication.api.callbacks.RegisterUserCallback;
import com.app_java_we.myapplication.api.callbacks.TurnsAvailableCallback;
import com.app_java_we.myapplication.api.domain.DoctorsBySpecialityData;
import com.app_java_we.myapplication.api.domain.DoctorsBySpecialityRequest;
import com.app_java_we.myapplication.api.domain.SpecialitiesData;
import com.app_java_we.myapplication.api.domain.SpecialitiesRequest;
import com.app_java_we.myapplication.models.Turn;
import com.app_java_we.myapplication.models.User;
import com.app_java_we.myapplication.repositories.DoctorsRepository;
import com.app_java_we.myapplication.repositories.SpecialtyRepository;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HospitalAPI {

    private Context mContext;
    private final String USER_DATA = "currentUser";


//    10.0.2.2:8080 = localhost (ruteo anterior)
    public static final String BASE_URL = "http://ec2-18-228-197-210.sa-east-1.compute.amazonaws.com:8080/api/v1/";

    public HospitalAPI(Context mContext) {
        this.mContext = mContext;
    }

    public void loginUser(final String email, final String password, @Nullable final LoginUserCallback callback){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);

        Call<User> userCall = hospitalService.loginUser(email,password);

        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User currentUser = response.body();
                if (currentUser == null){
                    Toast.makeText(mContext, R.string.error_wrong_emailOrPassword ,Toast.LENGTH_SHORT).show();
                }else {
                    saveUserData(currentUser);
                    callback.onSuccess(currentUser);
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.onError(t);
            }
        });

    }

    public void registerUser(final String dni, final String password, final String firstName, final String lastName, @Nullable final RegisterUserCallback callback){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);

        Call<User> userCall = hospitalService.registerUser(dni,password,firstName,lastName);

        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User currentUser = response.body();
                if (currentUser != null){
                    saveUserData(currentUser);
                    callback.onSuccess(currentUser);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    private void saveUserData(User currentUser){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("id_user",currentUser.getId());
        editor.putString("fistName",currentUser.getFistName());
        editor.putString("lastName",currentUser.getLastName());
        editor.putString("username",currentUser.getUsername());
        editor.putString("token_user",currentUser.getToken());
        editor.commit();
    }

    public void getSpecialties(){

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        String tokenAccess = sharedPreferences.getString("token_user",null);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);

        Call<SpecialitiesRequest>  specialitiesCall = hospitalService.getSpecialties(tokenAccess);

        specialitiesCall.enqueue(new Callback<SpecialitiesRequest>() {
            @Override
            public void onResponse(Call<SpecialitiesRequest> call, Response<SpecialitiesRequest> response) {
                SpecialitiesRequest specialitiesRequest = response.body();
                if (specialitiesRequest != null){
                    SpecialitiesData specialitiesData = specialitiesRequest.getEmbedded();
                    SpecialtyRepository.getInstance(mContext).setSpecialities(specialitiesData.getSpecialities());
                }
            }

            @Override
            public void onFailure(Call<SpecialitiesRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getDoctorsBySpeciality(Long ID){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        String tokenAccess = sharedPreferences.getString("token_user",null);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);
        final Call<DoctorsBySpecialityRequest> doctorsBySpecialityCall = hospitalService.getDoctorsBySpecialtyID(ID.intValue(),tokenAccess);

        doctorsBySpecialityCall.enqueue(new Callback<DoctorsBySpecialityRequest>() {
            @Override
            public void onResponse(Call<DoctorsBySpecialityRequest> call, Response<DoctorsBySpecialityRequest> response) {
                DoctorsBySpecialityRequest doctorsBySpecialityRequest = response.body();
                if (doctorsBySpecialityCall != null){
                    DoctorsBySpecialityData doctorsBySpecialityData = doctorsBySpecialityRequest.getEmbedded();
                    DoctorsRepository.getInstance(mContext).setDoctorsByID(doctorsBySpecialityData.getDoctors());
                }
            }

            @Override
            public void onFailure(Call<DoctorsBySpecialityRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getDoctors(){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        String tokenAccess = sharedPreferences.getString("token_user",null);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);
        final Call<DoctorsBySpecialityRequest> doctorsBySpecialityCall = hospitalService.getDoctors(tokenAccess);

        doctorsBySpecialityCall.enqueue(new Callback<DoctorsBySpecialityRequest>() {
            @Override
            public void onResponse(Call<DoctorsBySpecialityRequest> call, Response<DoctorsBySpecialityRequest> response) {
                DoctorsBySpecialityRequest doctorsBySpecialityRequest = response.body();
                if (doctorsBySpecialityCall != null){
                    DoctorsBySpecialityData doctorsBySpecialityData = doctorsBySpecialityRequest.getEmbedded();
                    DoctorsRepository.getInstance(mContext).setDoctors(doctorsBySpecialityData.getDoctors());
                }
            }

            @Override
            public void onFailure(Call<DoctorsBySpecialityRequest> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getTurnsAvailableByDoctorAndDay(Long doctorID,String day,@Nullable final TurnsAvailableCallback callback){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        String tokenAccess = sharedPreferences.getString("token_user",null);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);
        final Call<ArrayList<Turn>> listCall = hospitalService.getTurnsAvailableByDoctorAndDay(tokenAccess,doctorID,day);
        listCall.enqueue(new Callback<ArrayList<Turn>>() {
            @Override
            public void onResponse(Call<ArrayList<Turn>> call, Response<ArrayList<Turn>> response) {
                ArrayList<Turn> turnList = response.body();
                if (turnList != null){
                    callback.onSuccess(turnList);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Turn>> call, Throwable t) {
                callback.onError(t);
            }
        });



    }

    public void postTurn(JsonObject newTurn, @Nullable final PostTurnCallback callback) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_DATA,Context.MODE_PRIVATE);
        String tokenAccess = sharedPreferences.getString("token_user",null);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);

        Call<Turn> turnCall = hospitalService.postTurn(tokenAccess,newTurn);

        turnCall.enqueue(new Callback<Turn>() {
            @Override
            public void onResponse(Call<Turn> call, Response<Turn> response) {
                Turn turn = response.body();
                if (turn != null){
                    callback.onSuccess();
                }else {
                    okhttp3.Response response1 = response.raw();
                    Toast.makeText(mContext, R.string.error_wrong_register, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Turn> call, Throwable t) {
                callback.onError(t);
            }
        });

    }

    public void getUserById(Long id_user,@Nullable final GetUserCallback callback) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HospitalService hospitalService = retrofit.create(HospitalService.class);
        final Call<User> userCall = hospitalService.getUserById(id_user);

        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User currentUser = response.body();
                if (currentUser != null){
                    callback.onSuccess(currentUser);
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.onError(t);
            }
        });
    }
}
