package com.app_java_we.myapplication.api.domain;

import com.google.gson.annotations.SerializedName;

public class SpecialitiesRequest {

    @SerializedName("_embedded")
    private SpecialitiesData embedded;

    public SpecialitiesData getEmbedded() {
        return embedded;
    }

    public void setEmbedded(SpecialitiesData embedded) {
        this.embedded = embedded;
    }
}
