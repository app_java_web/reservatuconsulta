package com.app_java_we.myapplication.api.domain;


import com.google.gson.annotations.SerializedName;

public class DoctorsBySpecialityRequest {

    @SerializedName("_embedded")
     private DoctorsBySpecialityData embedded;

    public DoctorsBySpecialityData getEmbedded() {
        return embedded;
    }

    public void setEmbedded(DoctorsBySpecialityData embedded) {
        this.embedded = embedded;
    }
}
