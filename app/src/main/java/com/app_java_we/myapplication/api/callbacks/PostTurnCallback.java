package com.app_java_we.myapplication.api.callbacks;

import android.support.annotation.NonNull;

public interface PostTurnCallback {
    void onSuccess();

    void onError(@NonNull Throwable throwable);

}
