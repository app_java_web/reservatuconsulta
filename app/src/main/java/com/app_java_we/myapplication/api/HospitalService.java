package com.app_java_we.myapplication.api;

import android.content.res.TypedArray;

import com.app_java_we.myapplication.api.domain.DoctorsBySpecialityData;
import com.app_java_we.myapplication.api.domain.DoctorsBySpecialityRequest;
import com.app_java_we.myapplication.api.domain.SpecialitiesRequest;
import com.app_java_we.myapplication.models.Turn;
import com.app_java_we.myapplication.models.User;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HospitalService {

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("login")
    Call<User> loginUser(@Field("username") String DNI, @Field("password") String password);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("register")
    Call<User> registerUser(@Field("username") String DNI, @Field("password") String password,@Field("firstName") String firstName,@Field("lastName") String lastName);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @GET("specialties")
    Call<SpecialitiesRequest> getSpecialties(@Header("Authorization") String Authorization);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @GET("doctorsBySpecialty")
    Call<DoctorsBySpecialityRequest> getDoctorsBySpecialtyID(@Query ("id") int id, @Header("Authorization") String Authorization);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @GET("doctors")
    Call<DoctorsBySpecialityRequest> getDoctors(@Header("Authorization") String Authorization);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @GET("turns_available_by_doctor")
    Call<ArrayList<Turn>> getTurnsAvailableByDoctorAndDay(@Header("Authorization") String Authorization, @Query("idDoctor") Long idDoctor, @Query("dia") String dia);

    @Headers("Content-Type:application/json")
    @POST("turns")
    Call<Turn> postTurn(@Header("Authorization") String Authorization, @Body JsonObject body);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @GET("users/{id}")
    Call<User> getUserById(@Path("id") Long id_user);
}
