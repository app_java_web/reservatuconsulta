package com.app_java_we.myapplication.models;

import java.io.Serializable;
import java.sql.Time;
import java.util.Objects;

public class Schedule implements Serializable {
    private Long id;
    private Day day;
    private Time hour_since;
    private Time hour_to;
    private Doctor doctor;

    public Schedule() {
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Time getHour_since() {
        return hour_since;
    }

    public void setHour_since(Time hour_since) {
        this.hour_since = hour_since;
    }

    public Time getHour_to() {
        return hour_to;
    }

    public void setHour_to(Time hour_to) {
        this.hour_to = hour_to;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(id, schedule.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
