package com.app_java_we.myapplication.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.api.callbacks.TurnsAvailableCallback;
import com.app_java_we.myapplication.fragments.DatePickerFragment;
import com.app_java_we.myapplication.models.Turn;
import com.app_java_we.myapplication.repositories.TurnRepository;

import java.util.ArrayList;

public class SelectDateActivity extends AppCompatActivity {

    private TextInputLayout textInputLayout_selectDate;
    private EditText editText_selectDate;
    private Button button_next;
    private HospitalAPI hospitalAPI;
    private final String USER_DATA = "currentUser";
    private TurnsAvailableCallback turnsAvailableCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_select_date);
        setSupportActionBar(toolbar);

        hospitalAPI = new HospitalAPI(getApplicationContext());
        textInputLayout_selectDate = (TextInputLayout) findViewById(R.id.textInputLayout_selectDate);
        editText_selectDate = (EditText) findViewById(R.id.editText_selectDate);
        editText_selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.editText_selectDate:
                        showDatePickerDialog();
                        break;
                }
            }
        });


        button_next = (Button) findViewById(R.id.button_next);

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptGetTurnsAvailable();
            }
        });

        turnsAvailableCallback = new TurnsAvailableCallback() {
            @Override
            public void onSuccess(@NonNull ArrayList<Turn> turns) {
                TurnRepository.getInstance(getApplicationContext()).setTurns(turns);
                Intent intent = new Intent(SelectDateActivity.this, SelectTurnActivity.class);
                SelectDateActivity.this.startActivity(intent);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                Toast.makeText(getApplicationContext(), R.string.error_turnNotAvailabe,Toast.LENGTH_SHORT).show();
            }
        };

    }

    private void attemptGetTurnsAvailable() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        Long id_doctor = sharedPreferences.getLong("id_doctor",-1);

        String dateSelected = editText_selectDate.getText().toString();

        Boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(dateSelected)){
            textInputLayout_selectDate.setError(getString(R.string.error_field_required));
            focusView = textInputLayout_selectDate;
            cancel = true;
        }
//        Date daySelected = null;
        if (cancel){
            focusView.requestFocus();
        }else {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            try {
//                daySelected = simpleDateFormat.parse(dateSelected);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("date_turn",dateSelected);
            editor.commit();
            hospitalAPI.getTurnsAvailableByDoctorAndDay(id_doctor,dateSelected,turnsAvailableCallback);
        }
    }


    private void showDatePickerDialog(){
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                String daySelected = String.valueOf(day);
                String monthSelected = String.valueOf(month+1);
//                final String selectDate = day + "/" + (month+1) + "/" + year;
                if (day <= 9){
                    daySelected = "0" + String.valueOf(day);
                }
                if ((month+1) <= 9){
                    monthSelected = "0" + String.valueOf((month+1));
                }

                final String selectDateFinal = year + "-" + monthSelected + "-" + daySelected;
                editText_selectDate.setText(selectDateFinal);
            }
        });

        datePickerFragment.show(getSupportFragmentManager(),"datePicker");
    }

}
