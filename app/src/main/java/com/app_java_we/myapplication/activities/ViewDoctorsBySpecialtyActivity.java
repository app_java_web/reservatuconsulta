package com.app_java_we.myapplication.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.models.Doctor;
import com.app_java_we.myapplication.repositories.DoctorsRepository;

import java.util.ArrayList;

public class ViewDoctorsBySpecialtyActivity extends AppCompatActivity {

    private ListView listView_doctors;
    private ArrayList<Doctor> doctors;
    private ArrayAdapter<Doctor> doctorArrayAdapter;

    private final String USER_DATA = "currentUser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_doctors);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_view_doctors);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        Long id_specialty = sharedPreferences.getLong("id_specialty",-1);

        listView_doctors = (ListView) findViewById(R.id.listView_doctorsBySpeciality);

        doctors = DoctorsRepository.getInstance(getApplicationContext()).getDoctorBySpeciality2(id_specialty);

        doctorArrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,doctors);
        listView_doctors.setAdapter(doctorArrayAdapter);
        doctorArrayAdapter.notifyDataSetChanged();

        listView_doctors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("id_doctor",doctors.get(position).getId());
                editor.commit();
                Intent intent = new Intent(ViewDoctorsBySpecialtyActivity.this, SelectDateActivity.class);
                ViewDoctorsBySpecialtyActivity.this.startActivity(intent);
            }
        });

    }

}
