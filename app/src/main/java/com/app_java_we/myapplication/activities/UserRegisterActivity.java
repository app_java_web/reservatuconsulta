package com.app_java_we.myapplication.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.api.callbacks.RegisterUserCallback;
import com.app_java_we.myapplication.models.User;

public class UserRegisterActivity extends Activity {

    private TextInputLayout textInputLayout_dniRegister;
    private EditText editText_dniRegister;
    private TextInputLayout textInputLayout_passwordRegister;
    private EditText editText_passwordRegister;
    private TextInputLayout textInputLayout_passwordTwoRegister;
    private EditText editText_passwordTwoRegister;
    private TextInputLayout textInputLayout_lastNameRegister;
    private EditText editText_lastNameRegister;
    private TextInputLayout textInputLayout_firstNameRegister;
    private EditText editText_firstNameRegister;
    private Button button_registerUser;

    private Context mContext;
    HospitalAPI hospitalAPI;
    RegisterUserCallback registerUserCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);

        textInputLayout_dniRegister = (TextInputLayout) findViewById(R.id.textInputLayout_dniRegister);
        textInputLayout_passwordRegister = (TextInputLayout) findViewById(R.id.textInputLayout_passwordRegister);
        textInputLayout_passwordTwoRegister = (TextInputLayout) findViewById(R.id.textInputLayout_passwordTwoRegister);
        textInputLayout_firstNameRegister = (TextInputLayout) findViewById(R.id.textInputLayout_firstNameRegister);
        textInputLayout_lastNameRegister = (TextInputLayout) findViewById(R.id.textInputLayout_lastNameRegister) ;
        editText_firstNameRegister = (EditText) findViewById(R.id.editText_firstNameRegister);
        editText_firstNameRegister = (EditText) findViewById(R.id.editText_lastNameRegister);
        editText_dniRegister = (EditText) findViewById(R.id.editText_dniRegister);
        editText_passwordRegister = (EditText) findViewById(R.id.editText_passwordRegister);
        editText_passwordTwoRegister = (EditText) findViewById(R.id.editText_passwordTwoRegister);
        button_registerUser = (Button) findViewById(R.id.button_registerUser);

        button_registerUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });

        hospitalAPI = new HospitalAPI(mContext);
        registerUserCallback = new RegisterUserCallback() {
            @Override
            public void onSuccess(@NonNull User user) {
                Intent intent = new Intent(UserRegisterActivity.this, MainActivity.class);
                finish();
                Toast.makeText(mContext, R.string.successful_register,Toast.LENGTH_SHORT).show();
                UserRegisterActivity.this.startActivity(intent);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                Toast.makeText(mContext, R.string.error_wrong_register,Toast.LENGTH_SHORT).show();
            }
        };

    }

    private void attemptRegister() {

        textInputLayout_dniRegister.setError(null);
        textInputLayout_passwordRegister.setError(null);
        textInputLayout_passwordTwoRegister.setError(null);

        Boolean cancel = false;
        View focusView = null;

        String userDNI = editText_dniRegister.getText().toString();
        String userPassword = editText_passwordRegister.getText().toString();
        String userPasswordTwo = editText_passwordTwoRegister.getText().toString();
        String firstName = editText_firstNameRegister.getText().toString();
        String lastName = editText_firstNameRegister.getText().toString();

        if (!userPassword.equals(userPasswordTwo)){
            textInputLayout_passwordRegister.setError(getString(R.string.error_password_doesntmatch));
            focusView = textInputLayout_passwordRegister;
            cancel = true;
        }

        if (!TextUtils.isEmpty(userPassword) && !isPasswordValid(userPassword)) {
            textInputLayout_passwordRegister.setError(getString(R.string.error_short_password));
            focusView = textInputLayout_passwordRegister;
            cancel = true;
        }

        if (!TextUtils.isEmpty(userPasswordTwo) && !isPasswordValid(userPasswordTwo)) {
            textInputLayout_passwordTwoRegister.setError(getString(R.string.error_short_password));
            focusView = textInputLayout_passwordTwoRegister;
            cancel = true;
        }

        if (TextUtils.isEmpty(userDNI)) {
            textInputLayout_dniRegister.setError(getString(R.string.error_field_required));
            cancel = true;
        } else if (!isDNIValid(userDNI)) {
            textInputLayout_dniRegister.setError(getString(R.string.error_invalid_dni));
            focusView = textInputLayout_dniRegister;
            cancel = true;
        }

        if (cancel){
            focusView.requestFocus();
        } else {
            hospitalAPI.registerUser(userDNI,userPassword,firstName,lastName,registerUserCallback);
        }
    }

    private boolean isDNIValid(String dni) { return dni.length() == 8; }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

}
