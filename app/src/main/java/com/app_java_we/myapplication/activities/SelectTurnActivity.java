package com.app_java_we.myapplication.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.api.callbacks.GetUserCallback;
import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.api.callbacks.PostTurnCallback;
import com.app_java_we.myapplication.models.Doctor;
import com.app_java_we.myapplication.models.Turn;
import com.app_java_we.myapplication.models.User;
import com.app_java_we.myapplication.repositories.TurnRepository;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class SelectTurnActivity extends AppCompatActivity {

    private HospitalAPI hospitalAPI;
    private final String USER_DATA = "currentUser";
    private ListView listView_turns;
    private ArrayList<Turn> turnArrayList;
    private ArrayAdapter arrayAdapter;
    private GetUserCallback getUserCallback;
    private PostTurnCallback postTurnCallback;


    private User currentUser = null;
    private Turn newTurn = null;
    private JsonObject jsonObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_turn);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_select_turn);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        final Long id_doctor = sharedPreferences.getLong("id_doctor",-1);
        final Long id_user = sharedPreferences.getLong("id_user",-1);

        listView_turns = (ListView) findViewById(R.id.listView_turns);

        turnArrayList = TurnRepository.getInstance(getApplicationContext()).getTurns();
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,turnArrayList);
        listView_turns.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
        hospitalAPI = new HospitalAPI(getApplicationContext());

        newTurn = new Turn();
        jsonObject = new JsonObject();

        postTurnCallback = new PostTurnCallback() {
            @Override
            public void onSuccess() {
                Intent intent = new Intent(SelectTurnActivity.this, MainActivity.class);
                finish();
                SelectTurnActivity.this.startActivity(intent);
                Toast.makeText(getApplicationContext(), R.string.success_postNewTurn ,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                throwable.printStackTrace();
            }
        };


        getUserCallback = new GetUserCallback() {
            @Override
            public void onSuccess(@NonNull User user) {
                currentUser = user;
                newTurn.setUser(currentUser);
//                hospitalAPI.postTurn(newTurn,postTurnCallback);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                throwable.printStackTrace();
            }
        };


        listView_turns.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String turnDate = turnArrayList.get(position).getDate();
                final String turnTime = turnArrayList.get(position).getHour();

                AlertDialog.Builder builder = new AlertDialog.Builder(SelectTurnActivity.this);
                    builder.setTitle(R.string.title_alertDialog_selectTurn)
                            .setMessage("Tu turno es el día "+turnDate +" a las "+turnTime)
                            .setPositiveButton(R.string.positiveButton_alertDialog_selectTurn, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    newTurn.setDate(turnDate);
//                                    newTurn.setHour(turnTime);
//                                    newTurn.setAttended(false);
////                                    hospitalAPI.getUserById(id_user, getUserCallback);
//                                    newTurn.setUser(new User(id_user));
//                                    newTurn.setDoctor(new Doctor((id_doctor)));
                                    jsonObject.addProperty("attended",false);
                                    jsonObject.addProperty("date",turnDate);
                                    jsonObject.addProperty("hour",turnTime);
                                    JsonObject doctorJSON = new JsonObject();
                                    doctorJSON.addProperty("id",id_doctor);
                                    jsonObject.add("doctor",doctorJSON);
                                    JsonObject userJSON = new JsonObject();
                                    userJSON.addProperty("id",id_user);
                                    jsonObject.add("user",userJSON);
                                    hospitalAPI.postTurn(jsonObject,postTurnCallback);

                                }
                            })
                            .setNegativeButton(R.string.negativeButton_alertDialog_selectTurn, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

    }

}
