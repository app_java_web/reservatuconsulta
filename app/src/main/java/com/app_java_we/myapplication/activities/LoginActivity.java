package com.app_java_we.myapplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.api.HospitalAPI;
import com.app_java_we.myapplication.api.callbacks.LoginUserCallback;
import com.app_java_we.myapplication.models.User;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout textInputLayout_email;
    private EditText editText_email;
    private TextInputLayout textInputLayout_password;
    private EditText editText_password;
    private Button button_login;
    private TextView textView_linkRegisterUser;


    Context mContext = this;
    private String bearerToken;
    HospitalAPI hospitalAPI;
    LoginUserCallback loginUserCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textInputLayout_email = (TextInputLayout) findViewById(R.id.textInputLayout_email);
        textInputLayout_password = (TextInputLayout) findViewById(R.id.textInputLayout_password);
        editText_email = (EditText) findViewById(R.id.editText_email);
        editText_password = (EditText) findViewById(R.id.editText_password);
        button_login = (Button) findViewById(R.id.button_login);
        textView_linkRegisterUser = (TextView) findViewById(R.id.textView_linkRegisterUser);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        textView_linkRegisterUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, UserRegisterActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        });

        hospitalAPI = new HospitalAPI(mContext);
        loginUserCallback = new LoginUserCallback() {
            @Override
            public void onSuccess(@NonNull User user) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                finish();
                LoginActivity.this.startActivity(intent);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                Toast.makeText(mContext, R.string.error_unexpected_login ,Toast.LENGTH_SHORT).show();
            }
        };

    }

    private void attemptLogin() {

        textInputLayout_email.setError(null);
        textInputLayout_password.setError(null);

        Boolean cancel = false;
        View focusView = null;

        String userEmail = editText_email.getText().toString();
        String userPassword = editText_password.getText().toString();

        if (!TextUtils.isEmpty(userPassword) && !isPasswordValid(userPassword)) {
            textInputLayout_password.setError(getString(R.string.error_short_password));
            focusView = textInputLayout_password;
            cancel = true;
        }

        if (cancel){
            focusView.requestFocus();
        } else {
            hospitalAPI.loginUser(userEmail,userPassword,loginUserCallback);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }


}
