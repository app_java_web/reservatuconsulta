package com.app_java_we.myapplication.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app_java_we.myapplication.R;
import com.app_java_we.myapplication.adapters.SpecialtyAdapter;
import com.app_java_we.myapplication.models.Speciality;
import com.app_java_we.myapplication.repositories.SpecialtyRepository;

import java.util.ArrayList;

public class ViewSpecialties extends AppCompatActivity {

    private ListView listView_specialties;
    private ArrayList<Speciality> specialities;
    private SpecialtyAdapter specialityArrayAdapter;

    private final String USER_DATA = "currentUser";

    private final String SLDSPC_TAG = "selectedSpeciality";
    private Bundle selectedActivityBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_specialties);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_view_specialties);
        setSupportActionBar(toolbar);

        listView_specialties = (ListView) findViewById(R.id.listView_specialties);


        specialities = SpecialtyRepository.getInstance(getApplicationContext()).getSpecialties();
        specialityArrayAdapter = new SpecialtyAdapter(this,R.layout.list_item_specialty,specialities);

        listView_specialties.setAdapter(specialityArrayAdapter);
        specialityArrayAdapter.notifyDataSetChanged();

        listView_specialties.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("id_specialty",specialities.get(position).getId());
                editor.commit();
                Intent intent = new Intent(ViewSpecialties.this, ViewDoctorsBySpecialtyActivity.class);
                ViewSpecialties.this.startActivity(intent);
            }
        });


    }

}
